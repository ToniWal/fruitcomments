import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from './components/home/home.component'
import {ContactComponent} from './components/contact/contact.component';

const routes: Routes = [
  {
    path: '',
    component:null
  },
  {
    path: 'contact',
    component: null
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
